<?php

function pinger_rules_data_info() {
  return array(
    'pinger_responses' => array(
      'label' => t('Pinger Response'),
      'parent' => 'entity',
      'group' => t('Pinger'),
    ),
  );
}

/**
* Implementation of hook_rules_event_info()
*/
function pinger_rules_event_info() {
 return array(
    'pinger_result_insert' => array(
      'label' => t('A site ping result is saved'),
      'group' => t('Pinger'),
      'variables' => array(
        'pinger_result' => array(
          'type' => 'pinger_responses',
          'label' => t('Pinger Response'),
        ),
      ),
    ),
  );
}

/**
 * Implements hook_rules_condition_info().
 *
 */
function pinger_rules_condition_info() {
  return array (
    'pinger_has_valid_result_condition' => array(
      'label' => t('Pinger received a valid 200 OK result'),
      'parameter' => array(
        'pinger_result' => array(
          'type' => 'pinger_responses',
          'label' => t('Pinger Response.'),
        ),
      ),
      'group' => t('Pinger'),
    ),
  );
}

/**
* Implementation of hook_rules_event_info()
*/
function pinger_rules_action_info() {
  return array(
    'pinger_return_ping_result_message' => array(
      'label' => t('Return Ping Result Message'),
      'group' => t('Pinger'),
      'named parameter' => FALSE,
      'base' => 'rules_action_pinger_return_result_message',
      'provides' => array(
        'pinger_site' => array(
          'type' => 'text',
          'label' => t('Site URL'),
        ),
        'response_code' => array(
          'type' => 'text',
          'label' => t('Response Code'),
        ),
        'response_time' => array(
          'type' => 'integer',
          'label' => t('Response Time'),
        ),
      ),
    ),
  );
}


/**
* Test if a status of 200 (valid) was not received from the site ping
*
* @param boolean
*/
function pinger_has_valid_result_condition($result) {
  if ($result->response_code == 200) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
* Return the pinger result code
*
* @param boolean
*/
function pinger_result_condition($result) {
  return $result->response_code;
}

/**
* Action:
* Returns the new process id
*/
function rules_action_pinger_return_result_message($args, $element) {
  $ping_result = $element->variables['pinger_result'];
  $site = pinger_site_load($ping_result->site_id);
  $ping_result->response_code .= ' ';  // Needed to append the space or nothing was returned for a result of 0
  return array(
    'pinger_site' => $site->url,
    'response_code' => $ping_result->response_code,
    'response_time' => number_format($ping_result->response_time,4)
  );
}
