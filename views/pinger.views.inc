<?php

/**
 * @file
 *   Views integration for pinger module
 */

/**
 * Implements hook_views_data().
 */
function pinger_views_data() {
  $data['pinger_sites']['table']['group'] = t('Pinger Sites');
  $data['pinger_sites']['table']['base'] = array(
    'field' => 'id',
    'title' => t('Pinger Site'),
    'help' => t('Sites.'),
  );

  $data['pinger_sites']['id'] = array(
    'title' => t('ID'),
    'help' => t('Pinger Site ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );

  $data['pinger_sites']['url'] = array(
    'title' => t('URL of customer site'),
    'help' => t('The url of the site.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['pinger_responses']['table']['group'] = t('Pinger Responses');
  $data['pinger_responses']['table']['base'] = array(
    'field' => 'id',
    'title' => t('Pinger Responses'),
    'help' => t('Sites.'),
  );

  $data['pinger_responses']['table']['join'] = array(
    'pinger_sites' => array(
      'left_field' => 'id',
      'field' => 'site_id',
      'type' => 'left',
      ),
  );

  $data['pinger_responses']['id'] = array(
    'title' => t('ID'),
    'help' => t('Pinger Result ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),

    'sort' => array(
      'handler' => 'views_handler_sort_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );

  $data['pinger_responses']['site_id'] = array(
    'title' => t('Pinger Site ID'),
    'help' => t('The local pinger site ID.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'relationship' => array(
        'base' => 'pinger_sites', // The name of the table to join with.
        'base field' => 'id', // The name of the field on the joined table.
        // 'field' => 'nid' -- see hook_views_data_alter(); not needed here.
        'handler' => 'views_handler_relationship',
        'label' => t('Relationship with the pinger sites table'),
        'title' => t('Relationship with the pinger sites table'),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['pinger_responses']['response_code'] = array(
    'title' => t('Response Code'),
    'help' => t('Http response code'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );
  $data['pinger_responses']['response_time'] = array(
    'title' => t('Response Time'),
    'help' => t('Duration of response'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );
  $data['pinger_responses']['timestamp'] = array(
    'title' => t('Response Timestamp'),
    'help' => t('Timestamp of http response'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'time_tracker_views_handler_filter_date',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );

  return $data;
}

/**
 * Implements of hook_date_api_tables().
 */
function pinger_date_api_tables() {
  return array('pinger_responses');
}

/**
 * Implements hook_date_api_fields().
 *
 * This is required to allow timetracker timestamp
 * as a date argument in views.
 */
function pinger_date_api_fields($field) {
  $values = array(
    'sql_type' => DATE_UNIX,
    'tz_handling' => 'site',
    'timezone_field' => '',
    'offset_field' => '',
    'related_fields' => array(),
    'granularity' => array('year', 'month', 'day', 'hour', 'minute', 'second'),
  );

  switch ($field) {
    case 'pinger_responses.timestamp':
      return $values;
  }
}

