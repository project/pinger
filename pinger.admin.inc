<?php
/**
 * @file
 * Administrative settings for pinger module.
 */

/**
 * Form builder for new site form.
 */
function pinger_site_add_form($form, $form_state){

  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#description' => t('Enter the ABSOLUTE URL of the site that you would like to monitor.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 5,
  );

  $sites = entity_load('pinger_site', FALSE);
  $rows = array();
  foreach($sites as $site){
    $row = array($site->url, l(t('Delete'), 'admin/config/services/pinger/' . $site->id . '/delete'));
    $rows[] = $row;
  }
  $header = array(t('Url'), t('Delete'));
  $table = array(
    'rows' => $rows,
    'header' => $header,
    'empty' => t('You are not monitoring any sites yet.'),
  );
  $list = theme('table', $table);

  $form['list'] = array(
    '#type' => 'markup',
    '#markup' => $list,
    '#weight' => 1000
  );

  return $form;
}

/**
 * Validation handler for new site form.
 */
function pinger_site_add_form_validate($form, &$form_state) {
  $vals = $form_state['values'];
  if (!valid_url($vals['url'], TRUE)) {
    # Re-validate the URL with a default protocol.
    $default_to_http = 'http://' . $vals['url'];
    if (!valid_url($default_to_http, TRUE)) {
      form_set_error('url', t('The url you entered is not valid.'));
    }
    else {
      # The default protocol validated, so update form values.
      $form_state['values']['url'] = $default_to_http;
    }
  }
  # Don't allow duplicate sites.
  $conditions = array(
    'url' => $form_state['values']['url'],
  );
  if (count(pinger_site_load_multiple($conditions))) {
    form_set_error('url', t('The url you entered already exists. Duplicates are not allowed.'));  
  }
}

/**
 * Submit handler for new site form.
 */
function pinger_site_add_form_submit($form, $form_state) {
  $vals = $form_state['values'];
  $url = $vals['url'];

  $pinger = entity_get_controller('pinger_site')->create();
  $pinger->url = $url;
  $site = pinger_site_save($pinger);
  if ($site) {
    drupal_set_message(t('The site has been saved.'));
  }
  else{
    drupal_set_message(t('There was an error.'));
  }
}


/**
 * Form builder for delete site form.
 */
function pinger_site_delete_form($form, $form_state, $pinger_site){

  $form['id'] = array(
    '#type' => 'value',
    '#value' => $pinger_site->id,
  );

  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#disabled' => TRUE,
    '#default_value' => $pinger_site->url,
    '#description' => t('Enter the ABSOLUTE URL of the site that you would like to monitor.'),
  );

  return  confirm_form($form, t('Are you sure you want to delete %url?', array('%url' => $pinger_site->url)), 'admin/config/services/pinger', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

function pinger_site_delete_form_submit($form, $form_state){
  $site = pinger_site_load($form_state['values']['id']);
  $site_id = $site->id;
  $url = $site->url;
  pinger_site_delete($site);

  db_delete('pinger_responses')
    ->condition('site_id', $site_id)
    ->execute();

  drupal_set_message(t('The site %url has been deleted.', array('%url' => $url)));
  drupal_goto('admin/config/services/pinger');
}

