<?php

/**
 * @file
 * Holds the classes for pinger module.
 */

/**
 * PingerControllerInterface definition.
 */
interface PingerControllerInterface
  extends DrupalEntityControllerInterface {
  public function create();
  public function save($entity);
  public function delete($entity);
}

/**
 * PingerSiteController extends DrupalDefaultEntityController.
 */
class PingerSiteController
  extends DrupalDefaultEntityController
  implements PingerControllerInterface {

  /**
   * Create and return a new entity_example_basic entity.
   */
  public function create() {
    $item = new stdClass();
    $item->type = 'pinger_site';
    $item->url = '';
    $item->id = 0;
    $item->is_new = 1;
    return $item;
  }

  /**
   * Saves the custom fields using drupal_write_record()
   */
  public function save($item) {
    if (empty($item->id) || $item->id == 0) {
      $item->created = time();
    }
    module_invoke_all('entity_presave', $item, 'pinger_site');
    $primary_keys = $item->id ? 'id' : array();
    drupal_write_record('pinger_sites', $item, $primary_keys);
    $invocation = 'entity_insert';
    if (!empty($primary_keys)) {
      $invocation = 'entity_update';
    }
    module_invoke_all($invocation, $item, 'pinger_site');
    return $item;
  }

  /**
   * Delete a single entity.
   */
  public function delete($item) {
    $this->delete_multiple(array($item));
  }

  /**
   * @param $ids
   *   An array of entity IDs or a single numeric ID.
   */
  public function delete_multiple($items) {
    $ids = array();
    if (!empty($items)) {
      $transaction = db_transaction();
      try {
        foreach ($items as $item) {
          // Invoke hook_entity_delete().
          module_invoke_all('entity_delete', $item, 'pinger_site');
          $ids[] = $item->id;
        }
        db_delete('pinger_sites')
          ->condition('id', $ids, 'IN')
          ->execute();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('pinger_site', $e);
        throw $e;
      }
    }
  }
}

/**
 * PingerResponseController extends DrupalDefaultEntityController.
 */
class PingerResponseController
  extends DrupalDefaultEntityController
  implements PingerControllerInterface {

  /**
   * Create and return a new entity_example_basic entity.
   */
  public function create() {
    $item = new stdClass();
    $item->type = 'pinger_response';
    $item->id = 0;
    $item->is_new = 1;
    return $item;
  }

  /**
   * Saves the custom fields using drupal_write_record()
   */
  public function save($item) {
    if (empty($item->id) || $item->id == 0) {
      $item->created = time();
    }
    module_invoke_all('entity_presave', $item, 'pinger_response');
    $primary_keys = $item->id ? 'id' : array();
    drupal_write_record('pinger_responses', $item, $primary_keys);
    $invocation = 'entity_insert';
    if (!empty($primary_keys)) {
      $invocation = 'entity_update';
    }
    module_invoke_all($invocation, $item, 'pinger_response');
    if (module_exists('rules')) {
      rules_invoke_event('pinger_result_insert', $item);
    }
    return $item;
  }

  /**
   * Delete a single entity.
   */
  public function delete($item) {
    $this->delete_multiple(array($item));
  }

  /**
   * @param $items
   *   An array of entity IDs or a single numeric ID.
   */
  public function delete_multiple($items) {
    $ids = array();
    if (!empty($items)) {
      $transaction = db_transaction();
      try {
        foreach ($items as $item) {
          // Invoke hook_entity_delete().
          module_invoke_all('entity_delete', $item, 'pinger_response');
          $ids[] = $item->id;
        }
        db_delete('pinger_responses')
          ->condition('id', $ids, 'IN')
          ->execute();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('pinger_response', $e);
        throw $e;
      }
    }
  }
}
